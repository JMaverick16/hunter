import argparse
import logging
import multiprocessing as mp
import signal
import time


import hunter.messages as m
import hunter.controller as c
import hunter.gcp_utils as gu
import hunter.utils as u


# Global variable such that it can be used in exit handler
global sm_parent_conn


def exit_gracefully(signum, frame) -> None:
    """Makes sure that the child processes in main get a chance to exit gracefully and kill their child processes."""
    global sm_parent_conn
    logging.info('Controller is trying to exit gracefully')
    sm_parent_conn.send(m.ExitSession())


if __name__ == '__main__':
    global sm_parent_conn
    signal.signal(signal.SIGINT, exit_gracefully)  # e.g. CTRL-C on Linux
    signal.signal(signal.SIGTERM, exit_gracefully)

    my_parser = argparse.ArgumentParser(description='Hunter runs FlightGear military scenarios over MP - controller')
    my_parser.add_argument('-i', dest='identifier', type=str,
                           help='the identifier of this instance will be used as prefix in callsigns for targets',
                           default='x',
                           required=False)
    my_parser.add_argument("-l", dest="logging_level",
                           help="set logging level. Valid levels are DEBUG, INFO (default), WARNING",
                           required=False)
    my_parser.add_argument('-c', dest='ctrl_callsign', type=str,
                           help='the callsign used for the controller in MP chat',
                           default=u.CTRL_CALLSIGN, required=False)
    my_parser.add_argument('-s', dest='scenario', type=str,
                           help='the scenario to run',
                           default='north_norway', required=False)
    my_parser.add_argument('-d', dest='scenario_path', type=str,
                           help='the path to the directory where the scenarios live',
                           required=True)
    my_parser.add_argument('-g', dest='gci', action='store_true',
                           help='GCI functionality',
                           default=False, required=False)
    my_parser.add_argument('-o', dest='hostile', action='store_true',
                           help='Run with a hostile environment - i.e. also simulated targets shoot back',
                           default=False, required=False)
    my_parser.add_argument("-m", dest="mp_server_host", type=str,
                           help='The qualified hostname of the multi-player server (e.g. 127.0.0.1) - always port 5000',
                           default=u.MP_SERVER_HOST_DEFAULT,
                           required=False)
    my_parser.add_argument('-x', dest='cloud', type=int,
                           help='Use cloud integration (default is 0 = none [run standalone without cloud provider])',
                           default=u.CloudIntegrationLevel.none.value, required=False)

    args = my_parser.parse_args()

    # first check cloud level
    try:
        cloud_integration_level = u.parse_cloud_integration_level_as_int(args.cloud)
    except ValueError:
        cloud_integration_level = u.CloudIntegrationLevel.none
        # logging is not available yet -> using print
        print('Could not interpret value for cloud integration: {}. Falling back to default',
              u.CloudIntegrationLevel.none.value)

    cloud_logging = True if cloud_integration_level > 0 else False

    # configure logging
    my_log_level = 'INFO'
    if args.logging_level:
        my_log_level = args.logging_level.upper()
    u.configure_logging(my_log_level, True, 'Controller', cloud_logging)
    logging.info('Cloud integration level used: %s', cloud_integration_level.name)

    if cloud_integration_level is not u.CloudIntegrationLevel.none:
        gu.check_minimal_gcp_env_variables()

    sm_parent_conn, sm_child_conn = mp.Pipe()

    controller = c.Controller(args.mp_server_host, sm_child_conn, args.ctrl_callsign,
                              args.identifier, args.gci, cloud_integration_level,
                              args.scenario_path, args.scenario, args.hostile)
    controller.daemon = False
    time.sleep(10)  # Just to give the controller a bit time to initialise and log messages - not really needed
    controller.start()
