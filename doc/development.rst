
===========
Development
===========

-----------------
Program Structure
-----------------
* Multi-processing over multi-threading has been chosen for several reasons, amongst others:

  * Possibility to combine pure MP assets with running / controlling FG-instances using the same processing model
  * Most processing is CPU bound and/or it is easier to understand sockets per process vs. per thread
  * Inter-process communication is more explicit using multi-processing - and therefore easier to understand
  * No hassle with Python's GIL
  * If a process (apart from the controller) crashes, then only one target is lost
  * A not verified believe that sockets either work better or can only work for several incoming and outgoing MP ports if done in seperate processes per MP object.


MPSessionManager manages the bi-directional communication between:

* the user interface and the session
* the session and targets

MPSessionManager is a sub-process of the main thread. Targets are sub-processes of the session. All communication is done with multiprocessing.Pipe channels.

Each session manager has one special MPTarget of type ``receiver``, which is not visible to attackers. The receiver registers with the MP server with the solve purpose of listening to all MP chat message. If a chat message contains shooting info, then it is sent to the session manager, which then re-directs for final interpretation to the specific target's damage processing.



-----------
Other Notes
-----------
* Moving target assets are hard-coded to send every 1 second. That should be good enough for simulation over MP both as a reaction to hits and for movement. Using 1 second generates less network traffic and is easier to use for calculating moving targets (that is also the reason why e.g. speed is configured in m/s.
* The targets do not have any properties telling whether they can be hit by e.g. air munition vs. ground ammunition or cannon. The attacking planes' logic is trusted. A target can be made invulnerable (damage returns always Health.missed) or the max_hp can just be set to a very high number (e.g. 1'0000'000 requiring over 100'000 hits by a cannon before getting broken, but still returning Health.hit)
* Towing targets: Using the towing plane's movement properties with a delay in the towed target did not work. The delay increases a bit over time due to processing time (maybe 0.01 seconds each time), which is enough to make it flickery (the towing plane is also not exact due to processing maybe every 1.01 seconds when it is assumed to be 1 second - plus the time it takes for the pipe between the processes to forward the message). Those two things together make it look wrong. Therefore, the towed plane just uses the same network, start point and start after a delay - using the tractor's (instead of its own) flight model parameters (cruise speed, acceleration, turn rate etc.). The pre-requisite for this is that the network is a directed (and closed loop) graph.
* Network objects: nodes are always geometry.WayPoints. There is no special object for edges, but there are can be two attributes, which are used in moving targets using trips: ``cost`` for pre-calculated weights for Dijkstra shortest path algorithms as well as ``max_speed`` in m/s. Networkx Dijkstra assumes 1 if the cost attribute is not present. If the max_speed attribute is not present or 0, then there are no speed limits.
* The generated road/rail networks are not directly using osm2city code for several reasons: First because a network should not be constrained to a tile and second because osm2city removes/simplifies some info (e.g. highway/rail types) and stuff (e.g. tunnels) and finally because it is in local coordinates and would complicate the logic in roads.py. Instead Hunter has its own logic, duplicates the logic to add additional nodes for elevations and then gets the elevation simply by probing directly on top of a osm2city scenery (incl. bridges) - apart from tunnels, where it interpolates start and stop for inbetween nodes (in the tunnel) - because tunnels are needed.

--------------------------
Updating the Documentation
--------------------------

Follow the following for the end-user part:

* Create the html files:
    * Change directory to ``hunter\doc``
    * Issue ``sphinx-build -b html . build``
* Deploy a new version to GCP App engine ``gcloud app deploy``
* Announce in the `OPRF forum article <http://opredflag.com/forum_threads/3167237>`_.


-------------------
Cloud Setup / Notes
-------------------

Overall architecture:

* Controller and Worker(s) interact with each other using message passing (GCP Pub/Sub)
* Controller and Worker(s) store position updates directly in datastore
* Controller stores damage results directly in datastore
* Pages in web-app fetch updates of position updates and damage results on refresh or using ajax.
* The web-app is stateless, so there is no way to send updates to the web-app and be fetched from a global session
