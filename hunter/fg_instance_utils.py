import logging
import os
from os import path as op
import subprocess as sp
from typing import List

import hunter.geometry as g
import hunter.utils as u

WAIT_FG_READY = 30  # seconds to wait before Telnet attempts to connect to new FG instance
FRAME_RATE_THROTTLE = 10

HOST_FGFS = 'localhost'  # on which host the FG instances from the worker run


TERRASYNC_ARGS = ['--disable-terrasync']
LOGGING_ARGS = ['--log-level=warn']
VARIOUS_ARGS = ['--disable-splash-screen', '--disable-sound', '--disable-fullscreen',
                '--read-only',  # to be able to run several FG versions in parallel
                '--disable-gui'  # make sure the alert about several running versions in parallel is not shown
                ]
WEATHER_ARGS = ['--disable-real-weather-fetch', '--timeofday=noon', '--wind=0@0']
RENDERING_ARGS = ['--disable-random-objects', '--disable-random-vegetation', '--disable-random-buildings',
                  '--prop:/sim/rendering/texture-compression=off', '--prop:/sim/rendering/quality-level=0',
                  '--prop:/sim/rendering/shaders/quality-level=0',
                  '--disable-specular-highlight', '--disable-clouds', '--disable-clouds3d',
                  '--prop:/sim/rendering/draw-mask/clouds=false', '--fog-fastest',
                  '--prop:/sim/rendering/particles=0',
                  '--prop:/sim/rendering/multi-sample-buffers=1', '--prop:/sim/rendering/multi-samples=2',
                  '--disable-distance-attenuation', '--disable-horizon-effect']
NO_RENDER_ARGS = ['--prop:/sim/rendering/draw-mask/terrain=false', '--prop:sim/sceneryloaded-override=1',
                  '--prop:/sim/rendering/draw-mask/aircraft=false', '--prop:/sim/rendering/draw-mask/models=false']
# Even less: '--disable-hud', '--disable-panel']
AI_ARGS = ['--disable-ai-models', '--disable-ai-traffic', '--prop:/sim/traffic-manager/enabled=0',
           ' --prop:/sim/ai/enabled=0']


def make_command_line_automat(index: int, callsign: str, icao: str, props_string: str, show_world: bool) -> List[str]:
    cmd_args = list()
    cmd_args.append('--airport=' + icao)
    split_cmds = props_string.split(' ')
    cmd_args.extend(split_cmds)
    cmd_args.append(u.create_prop_for_int('forced-port', u.PORT_MP_BASIS_CLIENT_WORKER + index))
    return _make_command_line(index, cmd_args, 'automat', callsign, False, show_world, False, True, True)


def make_command_line_sam(index: int,
                          lon: float, lat: float, alt_m: float, heading: float,
                          aircraft: str,
                          callsign: str,
                          show_world: bool,
                          need_telnet: bool) -> List[str]:
    cmd_args = list()
    cmd_args.append('--lon=' + str(lon))
    cmd_args.append('--lat=' + str(lat))
    cmd_args.append('--altitude=' + str(g.metres_to_feet(alt_m)))
    cmd_args.append('--heading=' + str(heading))
    return _make_command_line(index, cmd_args, aircraft, callsign, False, show_world, False,
                              True, need_telnet)


def make_command_line_carrier(index: int,
                              lon: float, lat: float,
                              aircraft: str,
                              callsign: str,
                              show_world: bool,
                              need_telnet: bool) -> List[str]:
    cmd_args = list()
    cmd_args.append('--lon=' + str(lon))
    cmd_args.append('--lat=' + str(lat))
    cmd_args.append('--altitude=0')
    cmd_args.append('--heading=0')
    return _make_command_line(index, cmd_args, aircraft, callsign, False, show_world, True,
                              True, need_telnet)


def make_command_line_aircraft(index: int,
                               icao: str, runway: str,
                               aircraft: str,
                               callsign: str,
                               show_world: bool,
                               need_telnet: bool) -> List[str]:
    cmd_args = list()
    cmd_args.append('--airport=' + icao)
    cmd_args.append('--runway=' + runway)
    cmd_args.append('--enable-fuel-freeze')
    return _make_command_line(index, cmd_args, aircraft, callsign, False, show_world, True,
                              True, need_telnet)


def _make_command_line(index: int, specific_args: List[str],
                       aircraft: str,
                       callsign: str,
                       opfr_asset: bool, show_world: bool, use_real_weather: bool,
                       need_http: bool, need_telnet: bool) -> List[str]:
    cmd_args = list()
    cmd_args.append('./fgfs')
    cmd_args.append('--fg-root=' + os.environ['FG_ROOT'])
    cmd_args.extend(specific_args)
    fg_aircraft = '--fg-aircraft=' + os.environ['FG_AIRCRAFT']
    if opfr_asset:
        fg_aircraft = fg_aircraft + op.sep + aircraft
    cmd_args.append(fg_aircraft)
    cmd_args.append('--aircraft=' + aircraft)
    cmd_args.append('--terrasync-dir=' + os.environ['FG_TERRASYNC'])
    cmd_args.append('--callsign=' + callsign)
    cmd_args.extend(TERRASYNC_ARGS)
    cmd_args.extend(LOGGING_ARGS)
    cmd_args.extend(VARIOUS_ARGS)
    if use_real_weather:
        cmd_args.append('--enable-real-weather-fetch')
        cmd_args.append('--timeofday=real')
    else:
        cmd_args.extend(WEATHER_ARGS)
    cmd_args.extend(RENDERING_ARGS)
    if show_world:
        cmd_args.append('--visibility=6000')
        cmd_args.append('--geometry=800x600')
    else:
        cmd_args.extend(NO_RENDER_ARGS)
        cmd_args.append('--geometry=640x480')
    cmd_args.extend(AI_ARGS)
    if need_http:
        cmd_args.append('--httpd=' + str(u.PORT_HTTPD_BASIS + index))
    if need_telnet:
        cmd_args.append('--telnet=' + str(u.PORT_TELNET_BASIS + index))
    cmd_args.append('--allow-nasal-from-sockets')
    cmd_args.append('--verbose')
    logging.info(' '.join(cmd_args))
    return cmd_args


def run_instance(cmd_args: List[str]) -> None:
    _ = sp.Popen(cmd_args,
                 cwd=os.environ['FG_BIN'],
                 stdout=sp.PIPE, stderr=sp.STDOUT,
                 universal_newlines=True, bufsize=1)
    # stdout, _ = process.communicate()
    # print('STDOUT:{}'.format(stdout))
