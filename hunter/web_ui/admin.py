"""Handles all pages within the administration area."""

from flask import Blueprint, render_template

from flask_menu import register_menu

bp_admin = Blueprint('admin', __name__, url_prefix='/admin')


TITLE_USER_ADMIN = 'User Administration'


@bp_admin.route('user_admin')
@register_menu(bp_admin, '.admin', TITLE_USER_ADMIN, order=2)
def user_admin():
    return render_template('admin/user_admin.html', title=TITLE_USER_ADMIN)
