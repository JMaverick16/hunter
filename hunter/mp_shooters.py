"""Models a shooting MP target.

Some resources (most of them actually not used in calculations):
A https://apps.dtic.mil/dtic/tr/fulltext/u2/a426717.pdf: Limitations of guns as defence against manoeuvring air
B https://apps.dtic.mil/dtic/tr/fulltext/u2/a166753.pdf: The Development of Soviet Air Defense Doctrine and Practice
C https://archive.org/details/DTIC_ADA392785/page/n3/mode/2up: Soviet ZSU-23-4: Capabilities and Countermeasures
D https://www.armedconflicts.com/CZE-SVK-ZU-23-2-M2-modernizace-t94983: smaller gun than Shilka
"""
import math
import time
from dataclasses import dataclass
import logging
from typing import Any, Dict
import unittest

import hunter.emesary as e
import hunter.emesary_notifications as en
import hunter.geometry as g
import hunter.messages as m
import hunter.utils as u


def _process_outgoing_armament_notification_cannon(index: int, shell_id: int, number_of_hits: int,
                                                   remote_callsign: str) -> bytes:
    encoded_notification = en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(index, 4)
    encoded_notification += en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(en.ArmamentNotification_Id, 1)
    encoded_notification += en.SEPARATOR_CHAR
    secondary_kind = shell_id * -1 - 1
    notification = en.ArmamentNotification('mp-bridge', en.KIND_COLLISION, secondary_kind)
    notification.distance = number_of_hits
    notification.remote_callsign = remote_callsign
    encoded_notification += notification.encode_to_message_body()
    encoded_notification += en.MESSAGE_END_CHAR
    return encoded_notification


def _calc_ammo_flight_time(distance: float, muzzle_velocity: int) -> float:
    """Primitive unrealistic linear function between velocity and distance.
    Should be replaced by a something a bit more realistic: distance is not straight and velocity is reduced over
    time due to drag.
    """
    return distance / muzzle_velocity


@dataclass(frozen=True)
class ShootInstance:
    time_stamp: float
    callsign: str


class BulletShooter:
    __slots__ = ('parent', 'tracking_dist', 'max_shooting_dist', 'max_shooting_angle', 'min_shooting_angle',
                 'muzzle_velocity',
                 'bullets_per_burst', 'bullets_total', 'bullets_current',
                 'reload_time', 'reload_last_time',
                 'bullet_index', 'damage_probability_bullet',
                 'radar_visible_dist', 'max_consecutive_bursts',
                 'tracked_attacker', 'tracked_since', 'shooting_history', 'emesary_send_index', 'consecutive_bursts',
                 'target_acquired', 'last_shot_triggered',
                 'min_target_acquisition_time', 'pause_between_consecutive_bursts',
                 'turret_altitude_speed', 'turret_azimuth_speed',
                 'encoded_notification', 'shooting', 'turret_azimuth', 'turret_altitude')

    def __init__(self, parent,  # mpt.MPTarget not imported due to circular reference
                 tracking_dist: int, max_shooting_dist: int, max_shooting_angle: int,
                 min_shooting_angle: int, muzzle_velocity: int,
                 bullets_per_burst: int, bullets_total: int, reload_time: int,
                 bullet_index: int, damage_probability_bullet: float, radar_visible_dist: int,
                 max_consecutive_bursts: int) -> None:
        self.parent = parent

        self.tracking_dist = tracking_dist  # how far away are even trying to track a plane
        self.max_shooting_dist = max_shooting_dist
        self.max_shooting_angle = max_shooting_angle  # 90 degrees is straight up
        self.min_shooting_angle = min_shooting_angle  # negative is below horizon
        self.muzzle_velocity = muzzle_velocity  # m/s

        self.bullets_per_burst = bullets_per_burst
        self.bullets_total = bullets_total
        self.bullets_current = 0  # set to zero at the beginning, so we have a startup time

        self.reload_time = reload_time  # the time it takes to reload and prepare in seconds
        self.reload_last_time = 0  # when reloading was started

        self.bullet_index = bullet_index  # in damage.py -> shells_em
        # how many bullets does it take for a kill in average - based on shells_em in damage.nas
        self.damage_probability_bullet = damage_probability_bullet
        self.radar_visible_dist = radar_visible_dist  # from how far away the radar is detectable
        self.max_consecutive_bursts = max_consecutive_bursts

        self.tracked_attacker = None  # attacker callsign
        self.tracked_since = 0  # time.time
        self.shooting_history = list()  #
        self.emesary_send_index = 1
        self.consecutive_bursts = 0
        self.target_acquired = False
        self.last_shot_triggered = 0  # cannot used shooting_history, because it might be emptied before

        # FIXME hardcoded stuff, which might be different for another bullet shooter
        self.min_target_acquisition_time = 10
        self.pause_between_consecutive_bursts = 8
        self.turret_altitude_speed = 15  # degrees per second the altitude of the turret cannon can be adjusted
        self.turret_azimuth_speed = 30  # degrees per second the azimuth of the turret can be adjusted

        # MP stuff
        self.encoded_notification = None
        self.shooting = False  # whether currently there is nozzle fire
        self.turret_azimuth = 0.  # North is 0 or 360. Degrees from the shooter to the target.
        self.turret_altitude = 0.  # Horizontal is 0 degrees, straight up is 90, straight down is -90

    @classmethod
    def create_shilka_shooter(cls, parent) -> 'BulletShooter':  # parent is mpt.MP_Target
        """Create the bullet shooting part for a https://en.wikipedia.org/wiki/ZSU-23-4_Shilka
        Mostly based on ref[C].
            * max shooting dist is based on note on page 16 regarding accuracy.
            * bullets per burst = 50 a bit higher than in the source, but in wikipedia 50 per belt
            * using the GSh-23
        """
        import hunter.damage
        shell_data = hunter.damage.shells_em['GSh-23']
        return cls(parent, 10000, 2500, 85, -10, 930, 50, 2000,
                   300,
                   shell_data[0], shell_data[1], 5000, 2)

    def process_attackers(self, attackers: Dict[str, g.PositionHistory]) -> None:
        now = time.time()
        self._check_for_hits(attackers, now)  # needs to be before next because looking back on previous shooting
        self._check_for_tracking_and_shooting(attackers, now)

    def _check_for_hits(self, attackers: Dict[str, g.PositionHistory], now: float) -> None:
        """Based on prev shooting of ammunition calculate hits and missed.
        If the fly time of the bullet is the same or longer than the distance, then calculate a probability for
        a hit and maybe send an ArmamentNotification.
        Once a hit probability has been calculated, then discard that part of the shooting history.

        Cannot track and hit more than one attacker even though bullets are some seconds on their way.
        Therefore it is safe to just take the callsign of the last hit and sum all bullets up.
        """
        total_number_of_bullets = 0
        attacker_callsign = None
        for shoot_instance in reversed(self.shooting_history):
            # is the attacker still alive?
            if shoot_instance.callsign not in attackers:
                self.shooting_history.remove(shoot_instance)
                continue
            # check whether ammunition has reached the attacker
            attacker_last_position = attackers[shoot_instance.callsign].last_position
            dist = g.calc_distance_pos_3d(self.parent.position, attacker_last_position)
            flight_time = _calc_ammo_flight_time(dist, self.muzzle_velocity)
            delta_time = now - shoot_instance.time_stamp
            if delta_time >= flight_time:  # bullet has reached (or passed) the attacker
                # calculate the probability of hit
                probability_kill = 0.
                if dist < self.max_shooting_dist:
                    # FIXME: hard coded for Shilka right now
                    probability_kill = 0.16 - 0.14 * dist/self.max_shooting_dist
                logging.info('Probability kill = %f', probability_kill)
                if probability_kill > 0.:
                    number_of_bullets = probability_kill / self.damage_probability_bullet
                    logging.info('Number of bullets: %f', number_of_bullets)
                    total_number_of_bullets += number_of_bullets
                    attacker_callsign = shoot_instance.callsign
                # remove from history because processed
                self.shooting_history.remove(shoot_instance)
                continue
            else:
                pass  # nothing to do - maybe in 1 second the bullets will have arrived
        # now do an armament notification for the total bullets
        if total_number_of_bullets < 0.33:
            total_number_of_bullets = 0
        elif total_number_of_bullets < 1.:
            total_number_of_bullets = 1
        else:
            total_number_of_bullets = int(total_number_of_bullets)
        if total_number_of_bullets > 0:
            self.emesary_send_index += 1
            encoded_msg = _process_outgoing_armament_notification_cannon(self.emesary_send_index,
                                                                         self.bullet_index,
                                                                         total_number_of_bullets,
                                                                         attacker_callsign)
            logging.info('Sending emesary for %s', attacker_callsign)
            self.encoded_notification = encoded_msg

    def _check_for_tracking_and_shooting(self, attackers: Dict[str, g.PositionHistory], now: float) -> None:
        # check whether we still can track and shoot
        if self.parent.health not in [m.HealthType.fit_for_fight, m.HealthType.hit]:
            self._untrack_attacker()
            return

        # do we have any ammo at all?
        if self.bullets_current < self.bullets_per_burst:
            if self.reload_last_time == 0:
                self.reload_last_time = now
                self._untrack_attacker()
                self.parent.radar_enabled = False
            elif self.reload_last_time + self.reload_time < now:
                self.reload_last_time = 0
                self.bullets_current = self.bullets_total
            return  # not ready now -> returning

        # check whether we still can track and shoot at the current chosen attacker
        closest_distance = 999999
        if self.tracked_attacker:
            if self.tracked_attacker not in attackers:
                self._untrack_attacker()
            else:
                closest_distance = g.calc_distance_pos_3d(self.parent.position,
                                                          attackers[self.tracked_attacker].last_position)
                if closest_distance > self.tracking_dist:
                    self._untrack_attacker()

        if self.tracked_attacker is None:
            candidate_attacker = None
            closest_distance = 999999
            for attacker_callsign, position_history in attackers.items():
                distance = g.calc_distance_pos_3d(self.parent.position, position_history.last_position)
                if distance < closest_distance and distance < self.tracking_dist:
                    closest_distance = distance
                    candidate_attacker = attacker_callsign
            if candidate_attacker:
                self._track_attacker(candidate_attacker, now)

        # handle radar
        if closest_distance < self.radar_visible_dist:
            self.parent.radar_enabled = True
        else:
            self.parent.radar_enabled = False

        # can we and shall we shoot at the attacker?
        self.shooting = False
        if self.tracked_attacker:
            # calculate relative orientations must be done first such that the turret is looking into the correct
            # direction as close as possible despite not shooting
            attacker_pos = attackers[self.tracked_attacker].last_position
            turret_alt_is_ready = self._adjust_turret_altitude(attacker_pos)
            turret_azi_is_ready = self._adjust_turret_azimuth(attacker_pos)

            # check whether angles within shooting parameters
            if turret_alt_is_ready is False or turret_azi_is_ready is False:
                return  # not ready to shoot

            # make sure target is acquired
            if not self.target_acquired:
                if now - self.tracked_since > self.min_target_acquisition_time:
                    self.target_acquired = True
                else:
                    return  # not yet ready to shoot again

            # we take some pauses between consecutive bursts such that the attacker does not know, that we exist
            # and where we are
            if self.consecutive_bursts >= self.max_consecutive_bursts:
                if now - self.last_shot_triggered > self.pause_between_consecutive_bursts:
                    self.consecutive_bursts = 0
                    self.last_shot_triggered = 0
                else:
                    return  # not yet ready to shoot again

            if closest_distance <= self.max_shooting_dist:
                logging.info('%s shoots at %s', self.parent.callsign, self.tracked_attacker)
                self.shooting_history.append(ShootInstance(time.time(), self.tracked_attacker))
                self.consecutive_bursts += 1
                self.last_shot_triggered = now
                self.shooting = True
                self.bullets_current -= self.bullets_per_burst
        else:
            pass  # we do nothing, not even adjust the turret into a neutral position

    def _track_attacker(self, callsign: str, now: float) -> None:
        self.tracked_attacker = callsign
        self.tracked_since = now

    def _untrack_attacker(self) -> None:
        self.tracked_attacker = None
        self.tracked_since = 0
        self.consecutive_bursts = 0
        self.target_acquired = False
        self.last_shot_triggered = 0

    def _adjust_turret_altitude(self, attacker_pos: g.Position) -> bool:
        """Adjusts the altitude of the turret cannon(s). Returns True if within shooting parameters."""
        turret_could_shoot = True
        if self.parent.position.lon == attacker_pos.lon and self.parent.position.lon == attacker_pos.lon:
            target_altitude = 90
        else:
            distance = g.calc_distance_pos(self.parent.position, attacker_pos)
            alt_delta = attacker_pos.alt_m - self.parent.position.alt_m
            target_altitude = math.degrees(math.atan(math.fabs(alt_delta) / distance))
            if alt_delta < 0:
                target_altitude *= -1
        # adjust the turret altitude
        if self.turret_altitude < target_altitude:
            if (target_altitude - self.turret_altitude) > self.turret_altitude_speed:
                self.turret_altitude += self.turret_altitude_speed
                turret_could_shoot = False
            else:
                self.turret_altitude = target_altitude
            if self.turret_altitude > self.max_shooting_angle:
                self.turret_altitude = self.max_shooting_angle
                turret_could_shoot = False
        elif self.turret_altitude > target_altitude:
            if (self.turret_altitude - target_altitude) > self.turret_altitude_speed:
                self.turret_altitude -= self.turret_altitude_speed
                turret_could_shoot = False
            else:
                self.turret_altitude = target_altitude
            if self.turret_altitude < self.min_shooting_angle:
                self.turret_altitude = self.min_shooting_angle
                turret_could_shoot = False
        return turret_could_shoot

    def _adjust_turret_azimuth(self, attacker_pos: g.Position) -> bool:
        """Adjusts the azimuth of the turret cannon(s). Returns True if within shooting parameters."""
        turret_could_shoot = True
        if self.parent.position.lon == attacker_pos.lon and self.parent.position.lon == attacker_pos.lon:
            # cannot define the direction and therefore do not move
            return True  # depending on the altitude parameters might still be able to shoot
        bearing_to_attacker = g.calc_bearing_pos(self.parent.position, attacker_pos)
        delta = g.calc_delta_bearing(self.turret_azimuth, bearing_to_attacker)
        if math.fabs(delta) > self.turret_azimuth_speed:
            turret_could_shoot = False
            if delta > 0:
                delta = self.turret_azimuth_speed
            else:
                delta = -1 * self.turret_azimuth_speed
        self.turret_azimuth += delta
        if self.turret_azimuth >= 360:
            self.turret_azimuth -= 360
        elif self.turret_azimuth < 0:
            self.turret_azimuth += 360
        return turret_could_shoot

    def extend_props_with_shooting_stuff(self, props: Dict[int, Any]) -> None:
        """Extend the properties to send to MP with shooting info - maybe including an Emesary notifications.
        This is a bit brittle, because process_attackers in Shooter is called from controller thread, while
        this is called from another thread.
        """

        if self.encoded_notification:
            props[u.MP_PROP_EMESARY_BRIDGE_BASE + 19] = self.encoded_notification
        # visible gun fire
        props[u.MP_PROP_GENERIC_INT_BASE + 1] = 1 if self.shooting else 0
        # turret position is relative to the vehicle
        delta = g.calc_delta_bearing(self.parent.orientation.hdg, self.turret_azimuth)
        if delta == 0:
            props[u.MP_PROP_GENERIC_FLOAT_BASE] = 0
        elif 0 < delta <= 180:  # if to the right, then negative values
            props[u.MP_PROP_GENERIC_FLOAT_BASE] = -1 * delta
        else:  # else to the left and positive values
            props[u.MP_PROP_GENERIC_FLOAT_BASE] = 360 - delta
        # gun altitude
        props[u.MP_PROP_GENERIC_FLOAT_BASE + 1] = self.turret_altitude


class TestEmesary(unittest.TestCase):
    def test_encode(self):
        outgoing_cannon = _process_outgoing_armament_notification_cannon(13, 9, 14, 'OPFOR50')
        self.assertEqual(outgoing_cannon, b'!\x83\x01\x01\x0e!\x96!\x87w\x83\x01\x83\x93\x83\x8aOPFOR50~',
                         'F-16 cannon outgoing with index 13 and 14 hits')

    def test_message(self):
        shooter_callsign = 'OPFOR41'
        target_callsign = 'HB-VANO'
        position = g.Position(8, 46, 2000)
        import hunter.fgms_io as fio
        import ext.fgms as f
        import hunter.mp_targets as mpt
        mp_target = mpt.MPTarget.create_target(mpt.MPTarget.SHILKA, position)
        mp_target.callsign = shooter_callsign
        sender = fio.FGMSSender(mp_target, 'localhost', 5000, 5001, 60)
        # ca. middle of run method of FGMSSender
        props = mp_target.prepare_mp_properties()
        number_of_bullets = 1
        encoded_msg = _process_outgoing_armament_notification_cannon(11111, 4, number_of_bullets, target_callsign)
        sender.mp_target.shooter.encoded_notification = encoded_msg
        sender.mp_target.shooter.extend_props_with_shooting_stuff(props)
        packet_position = fio.create_position_message_static(mp_target, properties=props)
        packet_header = fio.create_header_packet(mp_target, f.position_message_type_code, packet_position)

        udp_packet = packet_header.allData()

        # from Controller _receive_fgms_data(self, udp_packet) -> None:
        fgms_unpacked_data = fio.decode_fgms_data_packet(udp_packet, dict())

        # from Controller _process_emesary_notifications(...)
        decoded_notifications = dict()
        for value in fgms_unpacked_data.encoded_notifications.values():
            if value != '':
                # noinspection PyBroadException
                try:
                    decoded_notifications.update(en.process_incoming_message(value))
                except Exception:
                    logging.error('Cannot process emesary notification')

        for notification in decoded_notifications.values():
            self.assertEqual(target_callsign, notification.remote_callsign, 'Target callsign')
            self.assertAlmostEqual(notification.distance, number_of_bullets, 1, 'Number of bullets')
            break  # just process the first one
