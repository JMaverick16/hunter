import argparse
import logging
from math import fabs
import os.path as osp
import pickle
import sys

import networkx as nx

from osm2city import parameters
import osm2city.roads as r
import osm2city.static_types.osmstrings as s
import osm2city.static_types.enumerations as e
import osm2city.utils.coordinates as co
import osm2city.utils.osmparser as op
from osm2city.utils.utilities import check_boundary, parse_boundary, BoundaryError, FGElev

import hunter.geometry as g
from hunter.utils import get_scenario_resources_dir


def _process_osm_for_roads_network(file_name: str, min_highway_type: int, max_point_dist: int, hover: float,
                                   transform: co.Transformation) -> None:
    network = nx.Graph()

    osm_way_result = op.fetch_osm_db_data_ways_keys([s.K_HIGHWAY])
    osm_nodes_dict = osm_way_result.nodes_dict
    osm_ways_dict = osm_way_result.ways_dict
    logging.info("Number of ways before basic processing: %i", len(osm_ways_dict))
    my_ways = list()
    used_nodes = set()
    for way in osm_ways_dict.values():
        highway_type = e.highway_type_from_osm_tags(way.tags)
        if highway_type is None:
            continue
        elif highway_type.value < min_highway_type:
            continue
        my_ways.append(way)
        for ref in way.refs:
            used_nodes.add(ref)
    logging.info("Number of ways after basic processing: %i", len(my_ways))
    logging.info('Number of nodes actually used: %i out of %i', len(used_nodes), len(osm_nodes_dict))
    total_nodes = set(osm_nodes_dict.keys())
    unused_nodes = total_nodes.difference(used_nodes)
    for key in unused_nodes:
        del osm_nodes_dict[key]

    r.check_points_on_line_distance(max_point_dist, my_ways, osm_nodes_dict, transform)
    logging.info('Added %i nodes due to points on line check', len(osm_nodes_dict) - len(used_nodes))

    # Find the elevation as per FG scenery
    parameters.FG_ELEV_CACHE = False
    fg_elev = FGElev(None, 0)
    fg_elev.close()

    # Add all nodes to the graph as WayPoint
    way_points_dict = dict()  # osm_id, WayPoint
    for key, node in osm_nodes_dict.items():
        wp = g.WayPoint(key, node.lon, node.lat)
        wp.alt_m = fg_elev.probe_elev((node.lon, node.lat), True)
        wp.alt_m += hover
        way_points_dict[key] = wp
        network.add_node(wp)
    # Add edges and correct the altitude of WayPoints in tunnels
    for way in my_ways:
        tunnel_alt_m = way_points_dict[way.refs[0]].alt_m
        for index, ref in enumerate(way.refs):
            if index == 0:
                continue
            prev_wp = way_points_dict[way.refs[index - 1]]
            this_wp = way_points_dict[way.refs[index]]
            if s.is_tunnel(way.tags):
                this_wp.alt_m = tunnel_alt_m
            dist = g.calc_distance_wp_wp(prev_wp, this_wp)
            edge_cost = dist + fabs(prev_wp.alt_m - this_wp.alt_m) * 2
            network.add_edge(prev_wp, this_wp, cost=edge_cost)

    logging.info("Created network graph with {} nodes and {} edges".format(nx.number_of_nodes(network),
                                                                           nx.number_of_edges(network)))
    output_file = osp.join(get_scenario_resources_dir(), file_name)
    with open(output_file, 'wb') as file_pickle:
        pickle.dump(network, file_pickle)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Create road network for Hunter using OSM data \
    based on a lon/lat defined area")
    parser.add_argument("-f", "--file", dest="filename",
                        help="read parameters from fILE (e.g. params.ini)",
                        type=str, required=True)
    parser.add_argument("-o", "--output", dest="output",
                        help="write network to file (e.g. foo_netw.pkl)",
                        type=str, required=True)
    parser.add_argument("-b", "--boundary", dest="boundary",
                        help="set the boundary as WEST_SOUTH_EAST_NORTH like *9_47.0_11_48.5 (. as decimal)",
                        type=str, required=True)
    parser.add_argument("-s", dest="service_roads", action='store_true',
                        help="Use service roads as the lowest level of roads. Residential is default if not set.",
                        required=False)
    logging.basicConfig(level=logging.INFO)
    args = parser.parse_args()

    min_highway_type = e.HighwayType.service.value if args.service_roads else e.HighwayType.residential.value

    parameters.read_from_file(args.filename)
    try:
        boundary_floats = parse_boundary(args.boundary)
    except BoundaryError as be:
        logging.error(be.message)
        sys.exit(1)

    logging.info("Overall boundary {}, {}, {}, {}".format(boundary_floats[0], boundary_floats[1],
                                                          boundary_floats[2], boundary_floats[3]))
    check_boundary(boundary_floats[0], boundary_floats[1], boundary_floats[2], boundary_floats[3])
    parameters.set_boundary(boundary_floats[0], boundary_floats[1],
                            boundary_floats[2], boundary_floats[3])

    the_transformer = co.Transformation(parameters.get_center_global())

    _process_osm_for_roads_network(args.output, min_highway_type, 50, 1.0, the_transformer)
