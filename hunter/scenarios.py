from enum import IntEnum, unique
import logging
import os.path as osp
import pickle
import random
from typing import List, Optional, Tuple
import time

import networkx as nx

import hunter.fg_targets as fgt
import hunter.mp_targets as mpt
import hunter.geometry as g
import hunter.messages as m
import hunter.utils as u


class Airport:
    """An airport definition usable for automats and FGTargets"""
    def __init__(self, icao: str, name: str, runway: str, heading_magnetic_deg: float,
                 ground_elev_ft: float,
                 longitude_deg: float, latitude_deg: float,
                 beacons: List[g.WayPoint]) -> None:
        self.icao = icao
        self.name = name
        self.runway = runway
        self.heading_magnetic_deg = heading_magnetic_deg  # from /orientation
        self.ground_elev_ft = ground_elev_ft  # from /position
        self.longitude_deg = longitude_deg  # from /position
        self.latitude_def = latitude_deg  # from /position
        self.beacons = beacons
        if not self.beacons:
            raise ValueError('Departure beacons needs to have at least 1 item.')


LSMM = Airport('LSMM', 'Meiringen', '28', 274.16, 1897.94, 8.122357102, 46.74124602,
               [g.WayPoint(1000, 8.01, 46.75), g.WayPoint(1001, 7.8, 46.665), g.WayPoint(1002, 7.635, 46.715)])
ENAT = Airport('ENAT', 'Alta', '29', 289.43, -0.2, 23.389255, 69.972425,
               [g.WayPoint(1000, 23.075, 70.037), g.WayPoint(1001, 23., 70.2)])
# Tromsø for tanker
ENTC = Airport('ENTC', 'Tromsø Airport, Langnes', '01', 2.76, 15.65, 18.913071, 69.672764,
               [g.WayPoint(1000, 19.03, 69.75), g.WayPoint(1001, 19.86, 69.875), g.WayPoint(1002, 20., 70.)])

# LIPA for tanker Croatia
LIPA = Airport('LIPA', 'Aviano Air Base', '23', 225.23, 405.76, 12.6086007, 46.03870907,
               [g.WayPoint(1000, 12.5133, 45.800236),  # D191N
                g.WayPoint(1001, 12.45585556, 45.62141667),  # D191Y
                g.WayPoint(1002, 12.281389, 45.071806),  # CHI
                g.WayPoint(1003, 12.713333, 44.542500),  # BELOV
                g.WayPoint(1004, 13.463056, 44.416389)  # BAXON
                ])


APT_DAT_AIRPORT_FILE = 'airports.pkl'  # situated in directory scenario_resources and created by airports_io.py


class AptDatAirport:
    """Simplified airport to hold the minimal data from FG apt-dat.

    According to the x-plane definition 1 = land airport, 16 = sea base, 17 = heliport
    """
    __slots__ = ('icao', 'name', 'lon', 'lat', 'kind')

    def __init__(self, icao: str, name: str, lon: float, lat: float, kind: int) -> None:
        self.icao = icao
        self.name = name
        self.lon = lon
        self.lat = lat
        self.kind = kind


class StaticTarget:
    """A container for the properties of a static target.
    Missile_priority determines whether it might be run as an FG instance of the same type.
    0 means never be run as MP target.
    Other priorities can only be 1 (highest), 2 and 3.
    """
    __slots__ = ('target_type', 'position', 'heading', 'missile_priority')

    def __init__(self, target_type: str, position: g.Position, heading: float, missile_priority: int = 0) -> None:
        self.target_type = target_type
        self.position = position
        self.heading = heading
        self.missile_priority = missile_priority if 0 <= missile_priority < 4 else 3


@unique
class AutomatType(IntEnum):
    """Needs to follow the numbering in automat-set.xml"""
    f_16 = 0
    mig_29 = 1
    f_15 = 2
    su_27 = 3


class AutomatTarget:
    """A container to define an automat (shooting fighter jet on FG instance).

    The parameters match to a certain degree the capabilities of the automat (at beginning of automat-set.xml,
    as per 2020-05-23).
    Parameters mean the following:
    * plan: flight plan (must exist in sub-folder "Routes")
    * craft: type of aircraft (see AutomatType)
    * randomize_plan: if True then randomize the order of waypoints after the 3rd, but not the last
    * fire-first: whether the automat fires first (offensive) or only starts offensive if having been shot at
    * short_range_ammo_max: 0 if None, maximum of aircraft if True, randomized between 1 and max-1 (sidewinder)
    * long_range_ammo_max: ditto (AMRAAM)
    * cannon_ammo: 0 - 150 bullets
    * floor: minimum altitude (ft) if automate is engaged in combat - it will always try to fight above this altitude.
             NB: this can be different from the altitude used in the waypoints of the flight plan
    * floor_dive: Set floor-dive to something above highest mountain peaks.
                  Otherwise it might hit terrain when doing a split-S (because terrain radar will not help in due time).
    * start_speed: Ground speed in KT. If starting from start of runway (cf. plan) it must be set to 0
    * transponder: if transponder is set to False, it will make it tougher to find the automat in a dogfight
    * engage_range: within this distance the automat will engage in fighting
    * g_endurance_modern: set to True means that the pilot uses a modern g-suit (e.g 1997 checkbox  in F-16)
    * pause_after_crash: time in minutes before coming back online. 25 seconds will always be added to this.
    * wait_before_online: time in minutes (after a crash it takes pause_after_crash+wait_before_online)
    * number_of_lives: unlimited if 0, otherwise after a number of crashes the Automat is not restarted anymore
    * tacview: stores https://www.tacview.net/ data into $fghome/Export

    The following property available in the automat is set by the Controller at runtime:
    * forced-callsign
    * forced-port

    The following properties available in the automat are fixed:
    * callsign-override (true)
    * blufor (false)
    * bomber (false)
    * remember-aggression-minutes (10)
    * fuel-percent (100)
    * cruise-mach (0.8)
    * cruise-min-mach (0.6)
    * emergency (false)
    * ceiling (45000)
    * roll (30)
    * counter (2)
    * port-override (true)
    * server-oprf (true)
    """
    __slots__ = ('plan', 'craft', 'randomize_plan', 'fire_first',
                 'a9_ammo', 'a120_ammo', 'cannon_ammo',
                 'floor', 'floor_dive', 'start_speed', 'transponder', 'engage_range',
                 'g_endurance', 'pause_time_minutes', 'freeze_time_minutes', 'number_of_lives', 'tacview')

    def __init__(self, plan: str, craft: AutomatType, randomize_plan: bool, fire_first: bool,
                 short_range_ammo_max: Optional[bool], long_range_ammo_max: Optional[bool], cannon_ammo: int,
                 floor: int, floor_dive: int, start_speed: int, transponder: bool, engage_range: int,
                 g_endurance_modern: bool, pause_after_crash: int, wait_before_online: int,
                 number_of_lives: int = 0, tacview: bool = False) -> None:
        self.plan = plan
        self.craft = craft
        self.randomize_plan = randomize_plan
        self.fire_first = fire_first
        self.a9_ammo = 0  # short_range_ammo_max is None
        if short_range_ammo_max is True:
            self.a9_ammo = 2
        elif short_range_ammo_max is False:
            self.a9_ammo = 1
        self.a120_ammo = 0  # long_range_ammo_max is None
        max_a120 = 4 if self.craft in [AutomatType.f_16, AutomatType.mig_29] else 6
        if long_range_ammo_max is True:
            self.a120_ammo = max_a120
        elif long_range_ammo_max is False:
            self.a120_ammo = random.randint(1, max_a120 - 1)
        self.cannon_ammo = cannon_ammo if 0 < cannon_ammo < 151 else 150
        self.floor = floor
        self.floor_dive = floor_dive
        self.start_speed = start_speed if start_speed >= 0 else 0
        self.transponder = transponder
        self.engage_range = engage_range if 0 < engage_range < 100 else 50
        self.g_endurance = 30 if g_endurance_modern else 10
        self.pause_time_minutes = pause_after_crash if pause_after_crash >= 0 else 0
        self.freeze_time_minutes = wait_before_online if wait_before_online >= 0 else 0
        self.number_of_lives = number_of_lives
        self.tacview = tacview

    def create_properties_string(self, forced_callsign: str) -> str:
        props_string = list()
        props_string.append(u.create_prop_for_string('plan', self.plan))
        props_string.append(u.create_prop_for_bool('callsign-override', True))
        props_string.append(u.create_prop_for_string('forced-callsign', forced_callsign))
        props_string.append(u.create_prop_for_bool('port-override', True))
        props_string.append(u.create_prop_for_bool('server-oprf', True))
        props_string.append(u.create_prop_for_bool('blufor', False))
        props_string.append(u.create_prop_for_bool('fire-first', self.fire_first))
        props_string.append(u.create_prop_for_int('craft', self.craft.value))
        props_string.append(u.create_prop_for_bool('randomize-plan', self.randomize_plan))
        props_string.append(u.create_prop_for_int('a9-ammo', self.a9_ammo))
        props_string.append(u.create_prop_for_int('a120-ammo', self.a120_ammo))
        props_string.append(u.create_prop_for_int('floor', self.floor))
        props_string.append(u.create_prop_for_int('floor-dive', self.floor_dive))
        props_string.append(u.create_prop_for_int('start-speed', self.start_speed))
        props_string.append(u.create_prop_for_int('cannon-ammo', self.cannon_ammo))
        props_string.append(u.create_prop_for_bool('transponder', self.transponder))
        props_string.append(u.create_prop_for_bool('bomber', False))
        props_string.append(u.create_prop_for_int('remember-aggression-minutes', 10))
        props_string.append(u.create_prop_for_int('engage-range', self.engage_range))
        props_string.append(u.create_prop_for_int('fuel-percent', 100))
        props_string.append(u.create_prop_for_double('cruise-mach', 0.8))
        props_string.append(u.create_prop_for_double('cruise-min-mach', 0.6))
        props_string.append(u.create_prop_for_bool('emergency', False))
        props_string.append(u.create_prop_for_int('ceiling', 45000))
        props_string.append(u.create_prop_for_int('G-endurance', self.g_endurance))
        props_string.append(u.create_prop_for_int('roll', 30))
        props_string.append(u.create_prop_for_float('counter', 2.))
        props_string.append(u.create_prop_for_double('pause-time-minutes', self.pause_time_minutes))
        props_string.append(u.create_prop_for_double('freeze-time-minutes', self.freeze_time_minutes))
        props_string.append(u.create_prop_for_bool('tacview', self.tacview))
        return ' '.join(props_string)


class CarrierDefinition:
    """Defines the properties for one carrier.

    Description of parameters:
    * carrier_type: one of the (4) MP carriers - most often Vinson is the best choice due to size and having fleet
    * sail_area: a list of lon/lat confining the area where the carrier can sail (first != last node -> not closed)
                 The sail area should be a https://en.wikipedia.org/wiki/Convex_polygon and at least have one line
                 between 2 points, which is longer than 20 km. Remember that a carrier's turn radius is measured in
                 several kilometres and that it can steam at over 20 kts.
    * loiter_centre: a point around which the carrier could loiters if nothing to do - is also the start point and
                     often in the middle
    * use_tacan_callsign: use the TACAN (as defined in CarrierType.value) instead of a randomised callsign
    """
    __slots__ = ('carrier_type', 'sail_area', 'loiter_centre', 'use_tacan_callsign')

    def __init__(self, carrier_type: fgt.CarrierType, sail_area: List[Tuple[float, float]],
                 loiter_centre: Tuple[float, float], use_tacan_callsign: bool = True) -> None:
        self.carrier_type = carrier_type
        self.sail_area = sail_area
        self.loiter_centre = loiter_centre
        self.use_tacan_callsign = use_tacan_callsign
        if len(self.sail_area) < 3:
            raise ValueError('The sail area for the carrier must at least have 3 nodes')


class TankerDefinition:
    """Defines the properties for one tanker

    Typically the pattern network is at FL200.
    The airport definition includes waypoints out of the airport, after which the tanker just flies towards the closest
    point in the pattern network.
    """
    __slots__ = ('airport', 'tanker_network')

    def __init__(self, airport: Airport, network: nx.Graph) -> None:
        self.airport = airport
        self.tanker_network = network


class ScenarioContainer:
    """Container for all elements of a scenario.

    Description of Parameters:
    * ident: the identifier used in the _build_scenario_* method
    * name: the name of the scenario. Used in saving damage results from pilots hitting an asset
    * description: not used yet.
    * defending_callsigns: list of pilots working as OPFOR and therefore not attacked by assets
    * south_west: the South West corner of the scenario area (mostly for mapping purposes -> should include all
                  airports from which attackers or targets start/land -> "war area")
    * north_east: the North East corner of the scenario area
    * icao: "typical" airport in the scenario - default for automats, but they might take off from another place.
    * airports: can be loaded with the corresponding method from a pickled file based on FG apt-dat info and selected
                based on whether their location is within the south_west / north_east boundary
    * magnetic_declination: used for GCI (https://www.ngdc.noaa.gov/geomag/calculators/magcalc.shtml#declination)
    * gci_allowed_vehicles: None or list of names. If None, then all targets of type vehicle are used in GCI, otherwise
                            only those, whose names are in the list (e.g. [mpt.MPTarget.S_300, mpt.MPTarget.BUK_M2])
    * polling_freq: time in seconds to test whether a new heli or ship should be added

    Most targets are added with add_* methods. See the related description of the functions for parameters.
    NB: targets can only added as part of the scenario creation - not dynamically afterwards.
    And only once (i.e. you cannot add several automats by calling the method add_automats() several times, you need
    to have a final list of automats when calling the method.

    Depending on how many targets the worker node(s) of hunter can run, the parameters above are influenced as
    follows:
    * the carrier (only one) has the top priority
    * the tanker (only one) has the next priority
    * automats have next priority - as long as there is space on worker(s) they will be present
    * missile ships have second highest priority - ditto
    * if there is still space on worker(s), then randomly SAMs from static targets will be replaced by shooting SAMs

    E.g. there are 2 automats defined, ships_missile=2 and 1 carrier. In static targets there are 15 SAMs defined
    with different priorities. Now if Hunter is run with 7 slots for shooting stuff, then 2+2+1 slots would be taken
    by automats/ships/carrier. The 2 remaining slots would be randomly used to fill with shooting SAMs out of
    the 15 defined by priority (see 'missile_priority' in StaticTarget).
    """

    def __init__(self, ident: str, name: str, description: str, defending_callsigns: List[str],
                 south_west: Tuple[float, float], north_east: Tuple[float, float],
                 icao: str,
                 magnetic_declination: float = 0,
                 gci_allowed_vehicles: Optional[List[str]] = None,
                 polling_freq: int = 60) -> None:
        self.ident = ident
        self.name = name
        self.description = description
        self.defending_callsigns = defending_callsigns
        self.static_targets = list()
        self.automats = None

        self.ships_missile = 0
        self.ships_min = 0
        self.ships_initial = 0
        self.ships_network = None
        self.ship_assets = list()

        self.carrier_definition = None

        self.tanker_definition = None

        self.helis_min = 0
        self.helis_initial = 0
        self.helis_network = None
        self.heli_assets = list()

        self.drones_min = 0
        self.drones_initial = 0
        self.drones_network = None
        self.drones_assets = list()

        self.trip_targets = None

        # towed
        self.towed_number = 0
        self.towing_asset = None
        self.towed_dist = 0
        self.towed_speed = 0
        self.towed_network = None

        self.south_west = south_west
        self.north_east = north_east
        self.icao = icao
        self.airports = list()
        self.centre_position.alt_m = -10  # make sure it is out of reach
        self.magnetic_declination = magnetic_declination
        self.gci_allowed_vehicles = gci_allowed_vehicles
        self.polling_freq = polling_freq  # number of seconds before next_moving_xxx() should be called again

        self.scenario_started = 0  # time.time() when started

    @property
    def centre_lon_lat(self) -> List[float]:
        lon = self.north_east[0] - (self.north_east[0] - self.south_west[0]) / 2
        lat = self.north_east[1] - (self.north_east[1] - self.south_west[1]) / 2
        return [lon, lat]

    @property
    def centre_position(self) -> g.Position:
        lon_lat = self.centre_lon_lat
        return g.Position(lon_lat[0], lon_lat[1], 0.)

    def load_apt_dat_airports(self) -> None:
        # noinspection PyBroadException
        try:
            full_path = osp.join(u.get_scenario_resources_dir(), APT_DAT_AIRPORT_FILE)
            with open(full_path, 'rb') as file_pickle:
                all_airports = pickle.load(file_pickle)

            airports_in_boundary = list()
            for apt in all_airports:
                if self.south_west[0] <= apt.lon <= self.north_east[0] and \
                        self.south_west[1] <= apt.lat <= self.north_east[1]:
                    airports_in_boundary.append(apt)
            self.airports = airports_in_boundary
        except Exception:  # Pickle can throw pretty much any exception
            logging.exception('Could not read airports information from file')

    def add_static_targets(self, static_targets: List[StaticTarget]) -> None:
        """Add one or several static (not moving) targets to the scenario."""
        self.static_targets = static_targets

    def add_automats(self, automats: List[AutomatTarget]) -> None:
        """Add one or several automatically flying shooting fighter aircraft as a list of AutomatTargets."""
        self.automats = automats

    def add_helicopters(self, min_number: int, initial_number: int, network: nx.Graph,
                        assets: List[mpt.MPTargetAsset]) -> None:
        """Add a set of (non_shooting) helicopters, which navigate randomly from node to node in the given network.
        * min_number: how many helicopters shall there be at any time. If fewer helicopters are available,
                      then Hunter will re-spawn additional helicopters
        * initial_number: how many helicopter to start with. Helicopters are added at the beginning add one by one
                         according to parameter polling_freq in the scenario definition.
        * network: a closed non-directional graph of WayPoints.
        * assets: list of MPTargetAssets, which will be randomly assigned as actual targets
        """
        self.helis_min = min_number
        self.helis_initial = initial_number
        self.helis_network = network
        self.heli_assets = assets

    def add_drones(self, min_number: int, initial_number: int, network: nx.Graph,
                   assets: List[mpt.MPTargetAsset]) -> None:
        """Same as helicopters."""
        self.drones_min = min_number
        self.drones_initial = initial_number
        self.drones_network = network
        self.drones_assets = assets

    def add_ships(self, missile_ships: int, min_number: int, initial_number: int, network: nx.Graph,
                  assets: List[mpt.MPTargetAsset]) -> None:
        """Add a set of ships, which navigate randomly from node to node in the given network.

        Same parameters as for helicopters, apart from:
        * missile_ships: the number of missile frigates in FG instances
        """
        self.ships_missile = missile_ships
        if network is None and (missile_ships > 0 or min_number > 0):
            raise ValueError('Network for moving ships may not be None if at least 1 (missile) ship requested')
        self.ships_min = min_number
        self.ships_initial = initial_number
        self.ships_network = network
        self.ship_assets = assets

    def add_carrier(self, carrier: CarrierDefinition) -> None:
        """Add one (there can only be one aircraft carrier)."""
        self.carrier_definition = carrier

    def add_tanker(self, tanker: TankerDefinition) -> None:
        """Add one (there can only be one air to air refueler)."""
        self.tanker_definition = tanker

    def add_targets_with_trips(self, trip_targets: List[mpt.MPTargetTrips]) -> None:
        """Add targets which follow network paths in trips (random or routed based origin/destination as a list.
         Each target has its own network. There is a possibility to create convoys.
        """
        self.trip_targets = trip_targets

    def add_towed_targets(self, towed_number: int, towing_asset: Optional[mpt.MPTargetAsset], towed_dist: int,
                          towed_speed: int, towed_network: Optional[nx.DiGraph]) -> None:
        """Add one or more towed targets (a plane towing a target [e.g. delta or wind sack) in some distance.
        * towed_number: number of towed targets
        * towing_asset: list of MPTargets used to tow the towed targets (aka. tractor). Can be None if towed number is 0
        * towed_dist: the distance between the tractor and the towed targets
        * towed_speed: how fast the tractor and thereby the towed target move
        * towed_network: a closed directional graph of WayPoints. Can be None if no towed target requested
        """
        if towed_number > 0 and (towing_asset is None or towed_network is None):
            raise ValueError('Towing assets or towed network may not be empty if at least one towing is requested')
        self.towed_number = towed_number
        self.towing_asset = towing_asset
        self.towed_dist = towed_dist
        self.towed_speed = towed_speed
        self.towed_network = towed_network

    def stamp_scenario_as_started(self) -> None:
        """Sets the timestamp when the scenario was fully loaded and started.

        Used e.g. for delayed activations.
        """
        self.scenario_started = time.time()

    def reset_ships_initial(self, new_number: int) -> None:
        self.ships_initial = new_number

    def reset_helis_initial(self, new_number: int) -> None:
        self.helis_initial = new_number

    def next_moving_ship(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        yield_asset = False
        if self.ships_initial > 0:
            self.ships_initial -= 1
            yield_asset = True
        elif remaining_assets < self.ships_min:
            yield_asset = True
        if yield_asset:
            starting_point = random.choice(list(self.ships_network.nodes))
            asset = random.choice(self.ship_assets)
            return m.AddMovingTarget(asset, self.ships_network, starting_point)
        return None

    def next_moving_heli(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        """Yes, it is duplicated code - but expected that helis and ships will be handled differently in future."""
        yield_asset = False
        if self.helis_initial > 0:
            self.helis_initial -= 1
            yield_asset = True
        elif remaining_assets < self.helis_min:
            yield_asset = True
        if yield_asset:
            starting_point = random.choice(list(self.helis_network.nodes))
            asset = random.choice(self.heli_assets)
            return m.AddMovingTarget(asset, self.helis_network, starting_point)
        return None

    def next_moving_drone(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        """Yes, it is duplicated code - but expected that helis and ships will be handled differently in future."""
        yield_asset = False
        if self.drones_initial > 0:
            self.drones_initial -= 1
            yield_asset = True
        elif remaining_assets < self.drones_min:
            yield_asset = True
        if yield_asset:
            starting_point = random.choice(list(self.drones_network.nodes))
            asset = random.choice(self.drones_assets)
            return m.AddMovingTarget(asset, self.drones_network, starting_point)
        return None

    def next_trip_targets(self) -> Optional[List[mpt.MPTargetTrips]]:
        """Determine which trip targets' activation time has come, so they are run."""
        now = time.time()
        if self.trip_targets:
            activated_trip_targets = list()
            for target in reversed(self.trip_targets):
                if (now - self.scenario_started) > target.activation_delay:
                    activated_trip_targets.append(target)
                    self.trip_targets.remove(target)
            return activated_trip_targets
        return None


def load_scenario(scenario_name: str, path: str) -> ScenarioContainer:
    import importlib.util
    module_name = 'scenario_' + scenario_name
    spec = importlib.util.spec_from_file_location(module_name, osp.join(path, module_name + '.py'))
    scenario_module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(scenario_module)
    return scenario_module.build_scenario(path)


def load_network(file_name: str, path: str) -> nx.Graph:
    full_path = osp.join(path, file_name)
    with open(full_path, 'rb') as file_pickle:
        network = pickle.load(file_pickle)
    logging.info("Loaded network graph with {} nodes and {} edges".format(nx.number_of_nodes(network),
                                                                          nx.number_of_edges(network)))
    return network


def create_circle_points(lon_center: float, lat_center: float, radius: float, alt_m: int,
                         number_of_points: int) -> nx.DiGraph:
    """Creates a number of WayPoints clock-wise around a point starting in North."""
    graph = nx.DiGraph()
    prev_node = None
    first_node = None
    for i in range(number_of_points):
        bearing = i * 360/number_of_points  # now we look from the centre to the point
        point_lon, point_lat = g.calc_destination(lon_center, lat_center, radius, bearing)
        node = g.WayPoint(i, point_lon, point_lat, 0, alt_m)
        node.is_point_of_interest = True
        graph.add_node(node)
        if prev_node is None:
            prev_node = node
            first_node = node
        else:
            graph.add_edge(prev_node, node)
            prev_node = node
    graph.add_edge(prev_node, first_node)  # close the network
    return graph


# Callsigns need to have 2 digits at the end
OPFOR_defaults = ['OPFOR33',  # Rudolf
                  'OPFOR34',   # Kokos
                  'OPFOR42',  # Leto
                  'OPFOR69',  # vanosten
                  'OPFOR77',  # Pinto
                  'OPFOR78'  # swamp
                  ]


# Assets are randomly chosen. If they appear more often, then the probability gets higher
default_ships_list = [mpt.oprf_frigate, mpt.us_navy_lake_champlain, mpt.us_navy_normandy, mpt.us_navy_oliver_perry,
                      mpt.us_navy_san_antonio]
default_helis_list = [mpt.blue_tigre, mpt.blue_ch_47, mpt.red_mi_8, mpt.red_mi_24]
default_drones_list = [mpt.blue_mq_9]
