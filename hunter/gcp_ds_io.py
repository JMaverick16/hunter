"""
Module to interact with Google Cloud Firestore in Datastore mode.


Cf. Cloud Datastore:
    * https://cloud.google.com/datastore
    * https://googleapis.dev/python/datastore/latest/client.html
"""
from datetime import datetime as dt
import logging
import queue
import time
from typing import Dict, List, Optional, Union

import google.cloud.datastore as ds

import hunter.gcp_utils as gu
import hunter.geometry as g
import hunter.messages as m
import hunter.scenarios as s
import hunter.utils as u

PROP_CALLSIGN = 'callsign'
PROP_HEALTH = 'health'  # the value, not the name

KIND_HUNTER_SESSION = 'HunterSession'
PROP_SESSION_ID = 'session_id'  # is a str due to pubsub attributes limitations
PROP_SCENARIO_IDENT = 'scenario_ident'
PROP_STARTED_DT = 'started_dt'  # timestamp
PROP_STARTED_BY = 'started_by'
PROP_STOPPED_DT = 'stopped_dt'
PROP_STOPPED_BY = 'stopped_by'
PROP_MAX_IDLE_TIME = 'max_idle_time'  # seconds
PROP_MP_SERVER_HOST = 'mp_server_host'
PROP_NUMBER_OF_WORKERS = 'number_of_workers'
PROP_IDENTIFIER = 'identifier'
PROP_GCI = 'gci'

KIND_DAMAGE_RESULT = 'DamageResult'
PROP_DAMAGE_TIME = 'damage_time'
PROP_WEAPON_TYPE = 'weapon_type'
PROP_ATTACKER = 'attacker'  # MP callsign
PROP_ATTACKER_KIND = 'attacker_kind'
PROP_TARGET = 'target'  # MP callsign
PROP_TARGET_KIND = 'target_kind'
PROP_CIVILIAN = 'is_civilian'
PROP_IS_FG_TARGET = 'is_fg_target'

KIND_POSITION_UPDATE = 'PositionUpdate'
PROP_KIND = 'kind'  # the type of vehicle, aircraft, building etc.
PROP_POSITION_LON = 'lon'
PROP_POSITION_LAT = 'lat'
PROP_POSITION_ALT_M = 'alt_m'
PROP_HEADING = 'heading'
PROP_SPEED = 'speed'

MAX_BATCH_SIZE = 400

DEFAULT_USER = 'GCP VM Instance'


# ===================== Hunter Session and Stuff =======================================================================


# The current session stored on module level - be careful about its state!
current_hunter_session = None


def query_not_terminated_session(ds_client: ds.Client) -> Union[ds.Entity, int]:
    """Checks whether there is an existing session based on existence of timestamp in field 'stopped'.

    This must be done before a new session is created - amongst others because when a web initiated
    session is queried from the controller, then it will look for the timestamp also."""
    query = ds_client.query(kind=KIND_HUNTER_SESSION)
    query.add_filter(PROP_STOPPED_DT, '=', None)
    results = list(query.fetch())
    if len(results) == 1:
        return results[0]
    else:
        return len(results)


def valid_current_session(session: Union[ds.Entity, int]) -> bool:
    if isinstance(session, int):
        return False
    return True


def fetch_web_initiated_session(ds_client: ds.Client) -> ds.Entity:
    logging.info('Fetching web initiated session')
    result = query_not_terminated_session(ds_client)
    if isinstance(result, int):
        raise ValueError('Expected exactly 1 not stopped session. But there were %i not stopped sessions', result)
    return result


def register_new_session(ds_client: ds.Client, scenario: s.ScenarioContainer, mp_server_host: str,
                         gci: bool, ctrl_callsign: str, identifier: str,
                         user: Optional[str] = None) -> ds.Entity:
    logging.info('Registering new session for scenario %s on %s', scenario.ident, mp_server_host)
    result = query_not_terminated_session(ds_client)
    if isinstance(result, int) and result > 0:
        raise ValueError('Expected all sessions to be stopped. But there were %i not stopped sessions', result)
    if isinstance(result, ds.Entity):
        raise ValueError('Expected all sessions to be stopped. But there is 1 not stopped session')
    session_id = u.create_session_id()
    _delete_previous_position_updates(ds_client)

    started_by = DEFAULT_USER if user is None else user

    session = ds.Entity(ds_client.key(KIND_HUNTER_SESSION))
    session.update(
        {
            PROP_SESSION_ID: session_id,
            PROP_SCENARIO_IDENT: scenario.ident,
            PROP_STARTED_DT: dt.utcnow(),
            PROP_STARTED_BY: started_by,
            PROP_STOPPED_DT: None,  # must be set explicitly
            PROP_MAX_IDLE_TIME: u.SESSION_MAX_IDLE_TIME_DEFAULT,
            PROP_MP_SERVER_HOST: mp_server_host,
            PROP_GCI: gci,
            PROP_CALLSIGN: ctrl_callsign,
            PROP_IDENTIFIER: identifier
        }
    )
    ds_client.put(session)
    logging.info('Registered new session with id %s', session_id)
    return session


class HunterSession:
    def __init__(self, session_id: str, scenario: s.ScenarioContainer, started_dt: dt, started_by: str,
                 mp_server_host: str, gci: bool, ctrl_callsign: str, identifier: str) -> None:
        self.session_id = session_id
        self.scenario = scenario
        self.started_dt = started_dt
        self.started_by = started_by
        self.mp_server_host = mp_server_host
        self.gci = gci
        self.ctrl_callsign = ctrl_callsign
        self.identifier = identifier

    @classmethod
    def map_session_from_entity(cls, entity: ds.Entity) -> 'HunterSession':
        global current_hunter_session
        if current_hunter_session is not None and current_hunter_session.session_id == entity[PROP_SESSION_ID]:
            return current_hunter_session

        scenario = s.load_scenario(entity[PROP_SCENARIO_IDENT])
        scenario.load_apt_dat_airports()
        my_session = HunterSession(entity[PROP_SESSION_ID], scenario, entity[PROP_STARTED_DT],
                                   entity[PROP_STARTED_BY], entity[PROP_MP_SERVER_HOST], entity[PROP_GCI],
                                   entity[PROP_CALLSIGN], entity[PROP_IDENTIFIER])
        current_hunter_session = my_session  # if this is mapped, then we know that it is a fresh session
        return current_hunter_session


def _delete_previous_position_updates(ds_client: ds.Client) -> None:
    """Deletes all existing position update records, so only the positions for the current session are available.
    Makes queries easier (no filter on session_id) and there is no reason to pay for stale data.
    """
    query = ds_client.query(kind=KIND_POSITION_UPDATE)
    results = list(query.fetch())
    logging.info('Deleting %i previous position updates', len(results))
    keys_to_delete = list()
    for result in results:
        if len(keys_to_delete) > MAX_BATCH_SIZE:
            ds_client.delete_multi(keys_to_delete)
            keys_to_delete = list()
        keys_to_delete.append(result.key)
    if keys_to_delete:  # delete remaining
        ds_client.delete_multi(keys_to_delete)


def terminate_session(ds_client: ds.Client, user: Optional[str] = None) -> None:
    logging.info('Terminating session')
    result = query_not_terminated_session(ds_client)
    if isinstance(result, int):
        raise ValueError('Expected exactly 1 not stopped session. But there were %i not stopped sessions', result)

    stopped_by = DEFAULT_USER if user is None else user

    result.update(
        {
            PROP_STOPPED_DT: dt.utcnow(),
            PROP_STOPPED_BY: stopped_by
        }
    )
    ds_client.put(result)


# ===================== Damage =========================================================================================

def _register_damage_result(ds_client: ds.Client, damage_result: m.DamageResult, session_id: str) -> None:
    record = ds.Entity(ds_client.key(KIND_DAMAGE_RESULT))
    record.update(
        {
            PROP_DAMAGE_TIME: damage_result.damage_time,
            PROP_HEALTH: damage_result.health.value,
            PROP_WEAPON_TYPE: damage_result.weapon_type.value,
            PROP_ATTACKER: damage_result.attacker,
            PROP_ATTACKER_KIND: damage_result.attacker_kind,
            PROP_TARGET: damage_result.target,
            PROP_TARGET_KIND: damage_result.target_kind,
            PROP_CIVILIAN: damage_result.civilian,
            PROP_IS_FG_TARGET: damage_result.fg_target,
            PROP_SESSION_ID: session_id
         }
    )
    ds_client.put(record)


def query_damage_results(ds_client: ds.Client, session_id: Optional[str]) -> Optional[List[m.DamageResult]]:
    query = ds_client.query(kind=KIND_DAMAGE_RESULT)
    if session_id:
        query.add_filter(PROP_SESSION_ID, '=', session_id)
    else:
        current_session = query_not_terminated_session(ds_client)
        if valid_current_session(current_session):
            query.add_filter(PROP_SESSION_ID, '=', current_session[PROP_SESSION_ID])
        else:
            return None
    query.order = ['-' + PROP_DAMAGE_TIME]
    results = list(query.fetch(limit=50))

    damages = list()
    for entity in results:
        health_type = m.map_health_type(entity[PROP_HEALTH], True)
        weapon_type = m.map_weapon_type(entity[PROP_WEAPON_TYPE])
        damage = m.DamageResult(health_type, weapon_type, entity[PROP_ATTACKER], entity[PROP_ATTACKER_KIND],
                                entity[PROP_TARGET], entity[PROP_TARGET_KIND], entity[PROP_CIVILIAN],
                                entity[PROP_IS_FG_TARGET])
        damage.damage_time = entity[PROP_DAMAGE_TIME]
        damages.append(damage)
    return damages


# ===================== PositionUpdates ================================================================================

def _upsert_position_updates(ds_client: ds.Client, batch: m.PositionUpdatesBatch,
                             prev_records: Dict[str, ds.Entity]) -> None:
    """Inserts or updates positions based on the newest values.
    then compares the content with the saved information to delete those entities, which are not actual
    anymore (e.g. dead).
    """
    # first do the upsert
    data_records = list()
    batch_callsigns = set()
    inserted = 0
    updated = 0
    with ds_client.transaction():  # need a transaction if multiple mutations to same entity
        for pos_update in batch.position_updates:
            batch_callsigns.add(pos_update.callsign)
            if pos_update.callsign in prev_records:
                record = prev_records[pos_update.callsign]
                if record[PROP_HEALTH] == pos_update.health and record[PROP_SPEED] <= 0 and pos_update.speed <= 0:
                    continue  # no need to send something to datastore, which has not changed
                else:
                    updated += 1
            else:
                record = ds.Entity(ds_client.key(KIND_POSITION_UPDATE))
                prev_records[pos_update.callsign] = record
                inserted += 1
            record.update(
                {
                    PROP_CALLSIGN: pos_update.callsign,
                    PROP_KIND: pos_update.kind,
                    PROP_HEALTH: pos_update.health.value,
                    PROP_POSITION_LON: pos_update.position.lon,
                    PROP_POSITION_LAT: pos_update.position.lat,
                    PROP_POSITION_ALT_M: pos_update.position.alt_m,
                    PROP_HEADING: pos_update.heading,
                    PROP_SPEED: pos_update.speed
                }
            )
            data_records.append(record)
        ds_client.put_multi(data_records)
    logging.info('Upserted %i position updates (out of which %i inserts)', updated + inserted, inserted)
    # then do the deletions
    to_delete_callsigns = set()
    for callsign, record in prev_records.items():
        if callsign not in batch_callsigns:
            ds_client.delete(record.key)
            to_delete_callsigns.add(callsign)
    if to_delete_callsigns:
        for callsign in to_delete_callsigns:
            del prev_records[callsign]
        logging.info('Deleted %i callsigns from position updates', len(to_delete_callsigns))


def query_current_session_positions(ds_client: ds.Client) -> Optional[List[m.PositionUpdate]]:
    """The actual positions for the current session - can be empty.
    If there is no valid current session, then None is returned.
    """
    session = query_not_terminated_session(ds_client)
    if not valid_current_session(session):
        return None

    query = ds_client.query(kind=KIND_POSITION_UPDATE)
    query.order = [PROP_CALLSIGN]
    results = list(query.fetch(limit=200))  # basically all

    positions = list()
    for entity in results:
        health_type = m.map_health_type(entity[PROP_HEALTH], False)
        position = g.Position(entity[PROP_POSITION_LON], entity[PROP_POSITION_LAT], entity[PROP_POSITION_ALT_M])
        position_update = m.PositionUpdate(entity[PROP_CALLSIGN], entity[PROP_KIND], health_type, position,
                                           entity[PROP_HEADING], entity[PROP_SPEED])
        positions.append(position_update)
    return positions


# ===================== Other Stuff ====================================================================================

class GCPDSRunner(u.QueuedRunner):
    """A thread interacting with GCP Datastore asynchronously, so the main process' run() method does not get halted.
    """
    __slots__ = ('position_updates_prev_records', 'session_id')

    def __init__(self, session_id: str) -> None:
        super().__init__('GCPDSRunner')
        self.session_id = session_id
        self.position_updates_prev_records = dict()  # key = callsign, value = datastore entity

    def run(self) -> None:
        ds_client = None
        logging.info('GCPDCRunner started')
        while True:
            # noinspection PyBroadException
            try:
                if not ds_client:
                    ds_client = gu.create_datastore_client()
                while True:
                    try:
                        message = self.incoming_queue.get_nowait()
                        if isinstance(message, m.ExitSession):
                            try:
                                terminate_session(ds_client)
                            except ValueError as e:
                                logging.warning('Unable to update the session as stopped.', e)
                            logging.info('GCPDCRunner got exit command - exiting')
                            return
                        elif isinstance(message, m.DamageResult):
                            logging.debug('GCPDSRunner got a damage result')
                            _register_damage_result(ds_client, message, self.session_id)
                        elif isinstance(message, m.PositionUpdatesBatch):
                            logging.debug('GCPDSRunner got a a batch of position updates')
                            _upsert_position_updates(ds_client, message, self.position_updates_prev_records)
                    except queue.Empty:
                        break  # nothing to do as this just signals an empty queue and we are done polling for now
            except Exception:
                logging.exception('Some exception during GCPDSRunner.run() occurred. Continuing and see.')
            time.sleep(0.5)
